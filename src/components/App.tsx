// import React;
import * as React from "react";
import * as _ from "lodash";
//import typestyle
import { style } from "typestyle";
// import Map component
import Map from "./Map";

// define container style
const containerStyle = style({
  height: "100%",
  width: "100%",
  position: "absolute",
  top: 0,
  left: 0
})

// define state interface
interface AppState {
  error: any;
  loading: boolean;
  locations: Array<any>;
  search: string;
}


// This function returns a promise which implements a fetch inside
const fetchLocations = (url, headers) => {
  const promise = new Promise((resolve, reject) => {
    fetch(url, {
      headers: new Headers(headers)
    })
      // convert to json
      .then(res => res.json())
      .then(result => {
        // resolve promise with result
        resolve({
          locations: result.locations,
          err: null
        })
      },
        // Reject promise
        error => {
          reject({
            locations: [],
            err: error
          })
        }
      );
  });
  // return promise
  return promise;
}

const filterCollection = (collection, str) => {
  // clear empty spaces
  const valStr = str.toLowerCase().replace(/\s/g,'');
  // return entire collection if string is emptyString
  if(valStr === ""){
    return collection;
  }
  // return collection filter by name
  return _.filter(collection,
    loc => {
      const locStr = loc.name.toLowerCase().replace(/\s/g,'');
      return locStr.indexOf(valStr) > -1
    }
  )
}

// This component is the App's main container
export class App extends React.Component<{}, AppState> {
  // define constructor method
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.loadLocations = this.loadLocations.bind(this);
    this.state = {
      error: null,
      loading: true,
      locations: [],
      search: ""
    }
  }

  handleChange(event){
    // get value
    const val = event.target.value;
    this.setState({search: val});
  }

  // this function loads the locations using fetchLocations static function
  loadLocations(){
      fetchLocations("https://api.smartmileos.com/public/locations",
      {
        "x-smartmile-app-key": "73701487-d158-4d39-877e-a84bca984765"
      })
      .then((res: {locations: Array<any>, err: any}) => {
        // update state with fetched data
        this.setState({locations: res.locations, loading: false})
      })
      .catch(() => this.setState({error: true, locations:[]}))
  }

  // When component mounts fetch location and update state.locations
  componentDidMount() {
    this.loadLocations();
  }

  render() {
    // filter collection by search
    const filteredLocations = filterCollection(this.state.locations, this.state.search);
    // render "loading..." while locations are loaded
    if(this.state.loading){
      return <div>Loadin...</div>
    }
    return <div className={ containerStyle }>
        <label>
          Search:
          <input type="text" value={this.state.search} onChange={this.handleChange} />
        </label>
        <Map locations={filteredLocations} />
      </div>;
  }
}
