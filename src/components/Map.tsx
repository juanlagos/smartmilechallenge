import * as React from "react";
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import MarkerWrapper from "./Marker";
import * as _ from "lodash";

// Wrap GoogleMap with compose. This component renders a map and the markers as children components
const Map = compose(
  // define props
  withProps({
    // Define HOC props
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyD5ZsWU8s_0q2qw-pqwV5hQaq-QTIx4_R0",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>

  {
    let avgLat = 0;
    let avgLng = 0;
    // info is an array that stores extra information about locations
    let info = [];
    // map locations and filter null values. latLng is an array that stores the latitudes and longitudes
    const latLng = props.locations.map(loc => {
      // get lat and lng if they exist, return null otherwise
      const lat =  _.get(loc, "apt[0].UniAddress.latitude", null);
      const lng = _.get(loc, "apt[0].UniAddress.longitude", null);
      if(lat && lng){
        // add lat and lng
        avgLat += lat;
        avgLng += lng;
        // pick only necessary fields in order to avoid passing un-needed data to child component
        info.push(_.pick(loc.apt[0].UniAddress, "city", "line1", "line2"));
        return [lat, lng];
      };
      return null;
    }).filter(coor => coor !== null);

    // find center
    avgLat /= latLng.length;
    avgLng /= latLng.length;

    // Render Googlmap and render locations
    return(
    <GoogleMap
      defaultZoom={8}
      defaultCenter={{ lat: avgLat, lng: avgLng }}
    >
      {
        // map locations if there is any
        latLng.length > 0 && latLng.map((loc, idx) => {
              return <MarkerWrapper key={idx} loc={loc} info={info[idx]} />
        })
      }
    </GoogleMap>
  );
}
);


export default Map;
