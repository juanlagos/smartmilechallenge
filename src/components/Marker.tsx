import * as React from "react";
import { Marker, InfoWindow } from "react-google-maps"

import * as _ from "lodash";

// define component prop types
interface MarkerWrapperProps{
  key: number | string;
  loc: Array<number>;
  info: any;
}

// define state types
interface MarkerWrapperState{
    showInfo: boolean;
}

// this component renders a marker
export class MarkerWrapper extends React.Component<MarkerWrapperProps, MarkerWrapperState> {
  // define constructor method
  constructor(props) {
    super(props);
    this.state = {
      showInfo: false
    }
  }
  // hide and show info
  handleOnClick(val){
      this.setState({showInfo: val});
  }

  render() {
    return (
      <Marker
        onClick={() => this.handleOnClick(true)}
        position={{lat: this.props.loc[0], lng: this.props.loc[1]}}>
          {
            // if shoInfo is true then render InfoWindow component
            this.state.showInfo && <InfoWindow onCloseClick={() => this.handleOnClick(false)}>
              <div>
                <div>{this.props.info.city}</div>
                <div>{this.props.info.line1}</div>
                <div>{this.props.info.line2}</div>
              </div>
            </InfoWindow>
          }
      </Marker>
  );
  }
}


export default MarkerWrapper;
