import * as React from "react";
import * as ReactDOM from "react-dom";

// import main App component
import { App } from "./components/App";

// render App to DOM under div id="root"
ReactDOM.render(
    <App/>,
    document.getElementById("root")
);
